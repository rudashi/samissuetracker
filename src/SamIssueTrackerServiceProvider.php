<?php

namespace Totem\SamIssueTracker;

use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamIssueTracker\App\Observers\IssueObserver;
use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class SamIssueTrackerServiceProvider extends ServiceProvider
{

    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'sam-issue-tracker';
    }

    public function boot(): void
    {
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations',
            __DIR__ . '/resources/views'
        );
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang',  $this->getNamespace());
        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace().'.php'),
        ], $this->getNamespace().'-config');

        $this->registerEvents();
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());

        $this->configureBinding([
            \Totem\SamIssueTracker\App\Repositories\Contracts\CategoryRepositoryInterface::class => \Totem\SamIssueTracker\App\Repositories\CategoryRepository::class,
            \Totem\SamIssueTracker\App\Repositories\Contracts\IssueRepositoryInterface::class => \Totem\SamIssueTracker\App\Repositories\IssueRepository::class,
            \Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface::class => \Rudashi\JobTicket\Repositories\JobTicketRepository::class,
        ]);
    }

    private function registerEvents(): void
    {
        Issue::observe(IssueObserver::class);
    }

}
