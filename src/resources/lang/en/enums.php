<?php

use Totem\SamIssueTracker\App\Enums\IssueMorph;
use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Enums\Workplace;

return [

    Workplace::class => [

        Workplace::KM1_A         => __('KM-1 A'),
        Workplace::KM1_B         => __('KM-1 B'),
        Workplace::RICOH         => __('RICOH'),
        Workplace::MGI           => __('MGI'),
        Workplace::VARIO_PRINT   => __('VARIO PRINT'),
        Workplace::SCREEN_A      => __('SCREEN A'),
        Workplace::SCREEN_B      => __('SCREEN B'),
        Workplace::COLOR_STREAM  => __('COLOR STREAM'),
        Workplace::LAMINATION    => __('Lamination'),
        Workplace::SEWING        => __('Sewing'),
        Workplace::GUILLOTINE    => __('Guillotine'),
        Workplace::HORAUF        => __('HORAUF'),
        Workplace::DIAMANT       => __('DIAMANT'),
        Workplace::ALEGRO        => __('ALEGRO'),
        Workplace::BOOKLNE       => __('HUNKELER BOOKLNE'),
        Workplace::CUTTER        => __('CUTTER'),
        Workplace::HANDCRAFTS    => __('Handcraft / Spiral'),
        Workplace::WIRE_O        => __('Wire sewing'),
        Workplace::BQ_480        => __('BQ 480'),
        Workplace::BQ_270        => __('BQ 270'),
        Workplace::CORDOBA       => __('Petratto Cordoba'),
    ],

    StatusType::class => [
        StatusType::WAITING         => __('Waiting'),
        StatusType::IN_PROGRESS     => __('In progress'),
        StatusType::REFUSED         => __('Refused'),
        StatusType::COMPLETED       => __('Completed'),
    ],

    IssueMorph::class => [
        IssueMorph::BlockThicknessMeasurement   => __('Block thickness measurement'),
        IssueMorph::OrderPrintIssue             => __('Print Issue'),
        IssueMorph::OrderBinderyIssue           => __('Bindery Issue'),
    ],

];
