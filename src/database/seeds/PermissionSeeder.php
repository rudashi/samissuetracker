<?php

namespace Totem\SamIssueTracker\Database\Seeds;

use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamAcl\Database\PermissionTraitSeeder;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{

    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return array_merge([
            [
                'slug' => 'issues.view',
                'name' => 'Can View Issues',
                'description' => 'Can view issues list',
            ],
            [
                'slug' => 'issues.create',
                'name' => 'Can Create Issue',
                'description' => 'Can create new issue',
            ],
            [
                'slug' => 'issues.edit',
                'name' => 'Can Edit Issue',
                'description' => 'Can edit issue',
            ],
            [
                'slug' => 'issues.show',
                'name' => 'Can Show Issue',
                'description' => 'Can show issue',
            ],
            [
                'slug' => 'issues.delete',
                'name' => 'Can Delete Issue',
                'description' => 'Can delete issue',
            ],
        ], [Issue::technology_perm]);
    }

}
