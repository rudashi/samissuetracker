<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;
use Totem\SamIssueTracker\Database\Seeds\CategorySeeder;
use Totem\SamIssueTracker\Database\Seeds\PermissionSeeder;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try {
            Schema::create('issues_categories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->text('description')->nullable();
                NestedSet::columns($table);
                $table->integer('order')->nullable();
                $table->string('type')->index()->nullable();
            });

            Schema::create('issues', function (Blueprint $table) {
                $table->increments('id');
                $table->uuid('uuid')->unique();
                $table->timestamps();
                $table->integer('user_id')->unsigned();
                $table->integer('category_id');
                $table->string('number');
                $table->string('status')->nullable();
                $table->string('title')->nullable();
                $table->text('description')->nullable();
                $table->tinyInteger('priority')->default(0);

                $table->integer('order_id')->nullable();
                $table->string('order_number')->nullable();
                $table->string('order_name')->nullable();
                $table->string('order_customer')->nullable();

                $table->integer('workplace_id')->nullable();
                $table->json('payload')->nullable();
                $table->json('process')->nullable();
                $table->integer('assignee_id')->nullable()->unsigned();
                $table->string('type')->index()->nullable();
                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('no action')->onDelete('no action');
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => PermissionSeeder::class
        ]);

        Artisan::call('db:seed', [
            '--class' => CategorySeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        (new PermissionSeeder)->down();

        Schema::dropIfExists('issues_categories');
        Schema::dropIfExists('issues');
    }
}
