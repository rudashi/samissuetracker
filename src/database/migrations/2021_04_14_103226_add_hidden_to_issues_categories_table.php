<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHiddenToIssuesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try {
            if (!Schema::hasColumn('issues_categories', 'hidden')) {
                Schema::table('issues_categories', function (Blueprint $table) {
                    $table->tinyInteger('hidden')->default(0)->after('type');
                });
            }
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        if (Schema::hasColumn('issues_categories', 'hidden')) {
            Schema::table('issues_categories', function (Blueprint $table) {
                $table->dropColumn('hidden');
            });
        }
    }
}
