<?php

use Illuminate\Database\Migrations\Migration;
use Totem\SamIssueTracker\App\Model\Categories\OrderPrintIssue;
use Totem\SamIssueTracker\App\Model\Category;

class AddNewPrintIssue extends Migration
{

    public function up(): void
    {
        Category::create([
            'name' => 'niezgodna ilość naddatku',
            'description' => NULL,
            '_lft' => 11,
            '_rgt' => 12,
            'parent_id' => 2,
            'order' => 8,
            'type' => OrderPrintIssue::class,
        ]);
    }

    public function down(): void
    {
        Category::query()
            ->where('name', 'niezgodna ilość naddatku')
            ->where('parent_id', 2)
            ->where('type',OrderPrintIssue::class)
            ->delete();
    }

}
