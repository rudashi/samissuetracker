<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletesToIssuesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try{
            Schema::table('issues_categories', function (Blueprint $table) {
                $table->softDeletes();
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        Schema::table('issues_categories', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
