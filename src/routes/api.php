<?php

use Illuminate\Support\Facades\Route;
use Totem\SamIssueTracker\App\Controllers\CategoryControllers;
use Totem\SamIssueTracker\App\Controllers\IssueControllers;
use Totem\SamIssueTracker\App\Controllers\IssueOrderControllers;

Route::group(['prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::group(['prefix' => 'issues'], static function() {

            Route::get('/', [IssueControllers::class, 'index']);
            Route::post('/', [IssueControllers::class, 'create']);
            Route::get('{id}', [IssueOrderControllers::class, 'show']);
            Route::get('{uuid}', [IssueControllers::class, 'show']);
            Route::patch('{uuid}', [IssueControllers::class, 'update']);
            Route::delete('{uuid}', [IssueControllers::class, 'destroy']);

            Route::group(['prefix' => 'categories'], static function() {
                Route::get('/', [CategoryControllers::class, 'index']);
                Route::get('tree', [CategoryControllers::class, 'tree']);
                Route::post('tree', [CategoryControllers::class, 'rebuildTree']);
                Route::get('filters', [CategoryControllers::class, 'fetchFilters']);
                Route::get('{id}', [CategoryControllers::class, 'show']);
            });
        });

    });

});
