<?php

use Totem\SamIssueTracker\App\Enums\Workplace;

return [

    'emails' => [
        Workplace::KM1_A        => '',
        Workplace::KM1_B        => '',
        Workplace::RICOH        => '',
        Workplace::MGI          => '',
        Workplace::VARIO_PRINT  => '',
        Workplace::SCREEN_A     => '',
        Workplace::SCREEN_B     => '',
        Workplace::COLOR_STREAM => '',
        Workplace::LAMINATION   => '',
        Workplace::SEWING       => '',
        Workplace::GUILLOTINE   => '',
        Workplace::HORAUF       => '',
        Workplace::DIAMANT      => '',
        Workplace::ALEGRO       => '',
        Workplace::BOOKLNE      => '',
        Workplace::CUTTER       => '',
        Workplace::HANDCRAFTS   => '',
        Workplace::WIRE_O       => '',
        Workplace::BQ_480       => '',
        Workplace::BQ_270       => '',
        Workplace::CORDOBA      => '',

        Workplace::PRINT        => '',
        Workplace::BINDERY      => '',

        'technology'            => '',
    ],

];
