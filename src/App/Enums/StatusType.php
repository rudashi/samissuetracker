<?php

namespace Totem\SamIssueTracker\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;
use Totem\SamCore\App\Traits\SelectableEnum;

class StatusType extends Enum implements LocalizedEnum
{
    use SelectableEnum;

    public const WAITING        = 'waiting';
    public const COMPLETED      = 'finished';
    public const REFUSED        = 'refused';
    public const IN_PROGRESS    = 'in progress';

    public static function getLocalizationKey(): string
    {
        return 'sam-issue-tracker::enums.' . static::class;
    }

}
