<?php

namespace Totem\SamIssueTracker\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class IssueMorph extends Enum implements LocalizedEnum
{

    public const BlockThicknessMeasurement = \Totem\SamIssueTracker\App\Model\Issues\BlockThicknessMeasurement::class;
    public const OrderBinderyIssue = \Totem\SamIssueTracker\App\Model\Issues\OrderBinderyIssue::class;
    public const OrderPrintIssue = \Totem\SamIssueTracker\App\Model\Issues\OrderPrintIssue::class;

    public static function toSelectArray(): array
    {
        $array = static::toArray();
        $selectArray = [];

        foreach ($array as $key => $value) {
            $selectArray[$key] = static::getDescription($value);
        }

        return $selectArray;
    }

    public static function getLocalizationKey(): string
    {
        return 'sam-issue-tracker::enums.' . __CLASS__;
    }

}
