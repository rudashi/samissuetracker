<?php

namespace Totem\SamIssueTracker\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;
use JanRejnowski\SamDefects\App\Enums\DefectPlaceEnum;
use Totem\SamCore\App\Traits\SelectableEnum;

class DefectPlaceType extends Enum implements LocalizedEnum
{
    use SelectableEnum;

    public const Customer               = DefectPlaceEnum::Customer;
    public const SalesRepresentative    = DefectPlaceEnum::SalesRepresentative;
    public const Technology             = DefectPlaceEnum::Technology;
    public const Planing                = DefectPlaceEnum::Planing;
    public const Printing               = DefectPlaceEnum::Printing;
    public const Bindery                = DefectPlaceEnum::Bindery;
    public const Storehouse             = DefectPlaceEnum::Storehouse;
    public const Profis                 = DefectPlaceEnum::Profis;
    public const Nothing                = 0;

    public static function getLocalizationKey(): string
    {
        return 'sam-defects::enums.' . DefectPlaceEnum::class;
    }

}
