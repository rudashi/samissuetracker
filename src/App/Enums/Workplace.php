<?php

namespace Totem\SamIssueTracker\App\Enums;

use BenSampo\Enum\FlaggedEnum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static FlaggedEnum PRINT
 * @method static FlaggedEnum BINDERY
 */
class Workplace extends FlaggedEnum implements LocalizedEnum
{

    public const KM1_A          = 1 << 1;
    public const KM1_B          = 1 << 2;
    public const RICOH          = 1 << 3;
    public const MGI            = 1 << 4;
    public const VARIO_PRINT    = 1 << 5;
    public const SCREEN_A       = 1 << 6;
    public const SCREEN_B       = 1 << 7;
    public const COLOR_STREAM   = 1 << 8;
    public const LAMINATION     = 1 << 9;
    public const SEWING         = 1 << 10;
    public const GUILLOTINE     = 1 << 11;
    public const HORAUF         = 1 << 12;
    public const DIAMANT        = 1 << 13;
    public const ALEGRO         = 1 << 14;
    public const BOOKLNE        = 1 << 15;
    public const CUTTER         = 1 << 16;
    public const HANDCRAFTS     = 1 << 17;
    public const WIRE_O         = 1 << 18;
    public const BQ_480         = 1 << 19;
    public const BQ_270         = 1 << 20;
    public const CORDOBA        = 1 << 21;

    public const PRINT = self::KM1_A | self::KM1_B | self::RICOH | self::MGI | self::VARIO_PRINT | self::SCREEN_A | self::SCREEN_B | self::COLOR_STREAM;
    public const BINDERY = self::LAMINATION | self::SEWING | self::GUILLOTINE | self::HORAUF | self::DIAMANT | self::ALEGRO | self::BOOKLNE | self::CUTTER | self::HANDCRAFTS | self::WIRE_O | self::BQ_480 | self::BQ_270 | self::CORDOBA   ;

    public function getFlags(): array
    {
        $members = static::getInstances();
        $flags = [];

        foreach ($members as $member) {
            if ($member->value < $this->value && $this->hasFlag($member)) {
                $flags[] = ['value' => $member->value, 'text' => $member->description];
            }
        }

        return $flags;
    }

    public static function getLocalizationKey(): string
    {
        return 'sam-issue-tracker::enums.' . __CLASS__;
    }

}
