<?php

namespace Totem\SamIssueTracker\App\Enums;

use BenSampo\Enum\Enum;

class CustomFieldType extends Enum
{

    public const TEXT = 0;
    public const ENUM = 1;
    public const NUMBER = 2;
    public const TEXTAREA = 3;
    public const DESCRIPTION = 9;
    public const COLUMN = 20;

}
