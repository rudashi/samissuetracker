<?php

namespace Totem\SamIssueTracker\App\Observers;

use Illuminate\Http\Request;
use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Model\Issue;

class IssueObserver
{
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function saving(Issue $issue): void
    {
        $this->callDecorateEvent($issue, __FUNCTION__);
    }

    public function creating(Issue $issue): void
    {
        $issue->user_id = auth()->id();
        $issue->number  = $issue->getNewNumber();
        $issue->status  = StatusType::WAITING;

        $this->callDecorateEvent($issue, __FUNCTION__);
    }

    public function created(Issue $issue): void
    {
        if ($this->request->filled('files')) {
            $issue->attachFiles($this->request->input('files'));
        }
        $this->callDecorateEvent($issue, __FUNCTION__);
    }

    private function callDecorateEvent(Issue $issue, string $event): void
    {
        $category = $issue->loadMissing('category')->getRelation('category');
        $className = str_replace('\\Categories\\', '\\Issues\\', get_class($category));

        if (class_exists($className)) {
            $decorateClassName = $className;
        } else {
            $decorateClassName = get_class($issue).'s\\'.class_basename($category);
        }

        if (class_exists($decorateClassName)) {
            $issue->unsetRelation('category');
            $issue->decorateBuilder($decorateClassName);
            $issue->type = class_basename($decorateClassName);
            event("eloquent.{$event}: {$decorateClassName}", $issue);
        }
    }

}
