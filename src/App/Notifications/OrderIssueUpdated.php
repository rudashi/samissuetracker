<?php

namespace Totem\SamIssueTracker\App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class OrderIssueUpdated extends IssueNotificationBase
{

    public function toMail(): MailMessage
    {
        $message = explode("\n", $this->issue->process['description']);
        $mail = new MailMessage;
        $mail->subject('[SAM] Zgłoszenie '.$this->issue->number.' do zlecenia '.$this->issue->order_number.' - '.$this->issue->status->description)
            ->greeting('Witaj')
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Zgłoszenie do zlecenia <strong>'.$this->issue->order_number.'</strong> zostało <strong>zaktualizowane</strong>.') )
            ->line(new HtmlString('Status Twojego zgłoszenia to <strong>'.$this->issue->status->description.'</strong>.') )
            ->line('---')
        ;

        foreach ($message as $line) {
            $mail->line($line);
        }

        $mail->line('---')
            ->line('Aktualny stan realizacji zgłoszenia można sprawdzić na stronie')
            ->action('Sprawdź zgłoszenie', config('app.url').'/t/issues/'.$this->issue->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;

        return $mail;
    }

}
