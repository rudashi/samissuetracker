<?php

namespace Totem\SamIssueTracker\App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Totem\SamIssueTracker\App\Model\Issue;

abstract class IssueNotificationBase extends Notification implements ShouldQueue
{
    use Queueable;

    protected Issue $issue;

    public function __construct(Issue $issue)
    {
        $this->issue = $issue;
    }

    public function via(): array
    {
        return [
            'mail',
        ];
    }

    abstract public function toMail(): MailMessage;

    public function toDatabase(): array
    {
        return [
            'id'                => $this->issue->id,
            'uuid'              => $this->issue->uuid,
            'created_at'        => $this->issue->created_at,
            'number'            => $this->issue->number,
            'order_number'      => $this->issue->order_number,
            'order_name'        => $this->issue->order_name,
            'process'           => $this->issue->process,
            'status'            => $this->issue->status,
        ];
    }
}
