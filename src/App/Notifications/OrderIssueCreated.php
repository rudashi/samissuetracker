<?php

namespace Totem\SamIssueTracker\App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class OrderIssueCreated extends IssueNotificationBase
{

    public function toMail(): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Zgłoszenie '.$this->issue->number.' do zlecenia '.$this->issue->order_number)
            ->greeting('Witaj')
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Zgłoszenie do zlecenia <strong>'.$this->issue->order_number.'</strong> zostało <strong>utworzone</strong>.') )
            ->line('Aktualny stan realizacji zgłoszenia można sprawdzić na stronie')
            ->action('Sprawdź zgłoszenie', config('app.url').'/t/issues/'.$this->issue->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;
    }

}
