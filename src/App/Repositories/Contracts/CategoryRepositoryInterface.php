<?php

namespace Totem\SamIssueTracker\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface CategoryRepositoryInterface extends RepositoryInterface
{

    public function tree(): Collection;

    public function findByQuery(string $phrase = null, string $column = 'name', array $columns = ['*']): Collection;

    public function getTypes();

    public function rebuildTree(): void;

}
