<?php

namespace Totem\SamIssueTracker\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;
use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamIssueTracker\App\Requests\IssueRequest;

interface IssueRepositoryInterface extends RepositoryInterface
{

    public function findWithRelationsById($id = 0, array $relationships = [], array $columns = ['*']): Issue;

    public function storeNew(array $attributes): Issue;

    public function updateByUUID(IssueRequest $request, string $uuid = null): Issue;

    public function findByOrder(int $id): Collection;

    public function deleteUUID(string $uuid): Issue;

    public function allWithAncestors(): Collection;

}
