<?php

namespace Totem\SamIssueTracker\App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Totem\SamIssueTracker\App\Model\Category;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamIssueTracker\App\Repositories\Contracts\CategoryRepositoryInterface;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{

    public function model(): string
    {
        return Category::class;
    }

    public function tree(): Collection
    {
        return $this->model->where('hidden', 0)->orderBy('order')->get()->toTree();
    }

    public function findByQuery(string $phrase = null, string $column = 'name', array $columns = ['*']): Collection
    {
        return $this->model
            ->with('ancestors')
            ->when($phrase, static function (\Illuminate\Database\Eloquent\Builder $query) use ($column, $phrase) {
                $query->where($column, 'LIKE', "%$phrase%");
            })
            ->whereNotNull('type')
            ->where('hidden', 0)
            ->orderBy('parent_id')
            ->orderBy('order')
            ->get($columns)
            ->makeHidden('customFields');
    }

    public function getTypes()
    {
        return $this->model->distinct()->whereNotNull('type')->get(['id', 'type'])->mapToGroups(function ($item, $key) {
            return [$item['view_name'] => $item['id']];
        });
    }

    public function rebuildTree(): void
    {
        $this->model::query()->update(['_lft' => 0, '_rgt' => 0]);
        $this->model->rebuildTree(
            $this->model->orderBy('parent_id')->orderBy('order')->get()->toTree()->toArray()
        );
    }
}
