<?php

namespace Totem\SamIssueTracker\App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamIssueTracker\App\Repositories\Contracts\IssueRepositoryInterface;
use Totem\SamIssueTracker\App\Requests\IssueRequest;

/**
 * @property Builder|Issue model
 */
class IssueRepository extends BaseRepository implements IssueRepositoryInterface
{

    public function model(): string
    {
        return Issue::class;
    }

    public function allWithAncestors(): Collection
    {
        return $this->model
            ->with('category.ancestors')
            ->whereHas('category', function (Builder $query) {
                $query->where('hidden', 0);
            })
            ->get();
    }

    public function findWithRelationsById($id = 0, array $relationships = [], array $columns = ['*']): Issue
    {
        return $this->findBy('uuid', $id, $columns)->loadMissing($relationships);
    }

    public function findBy(string $attribute, string $value, array $columns = ['*']): Issue
    {
        $data = parent::findBy($attribute, $value, $columns);

        if ($data === null) {
            throw new RepositoryException(__('Given id :code is invalid or issue not exist.', ['code' => $value]), 404);
        }
        return $data;
    }

    public function storeNew(array $attributes): Issue
    {
        $model = $this->model;
        $model->fill($attributes);
        $model->save();

        return $model;
    }

    public function updateByUUID(IssueRequest $request, string $uuid = null): Issue
    {
        $model = ($uuid === null) ? $this->model : $this->findBy('uuid', $uuid);

        if ($model->type === $request->input('type') || Str::contains($model->type, '\\')) {
            $model->fill($request->validated());

            if ($model->save()) {
                return $model;
            }
        }
        throw new RepositoryException(__('There was a problem while saving. Report the problem to the administrator.'));
    }

    public function findByOrder(int $id): Collection
    {
        $data = $this->model->with('category')->whereHas('category')->where('order_id', $id)->get();

        if ($data->isEmpty()) {
            throw new RepositoryException(__('No issues were found for the given order.'), 404);
        }

        return $data;
    }

    public function deleteUUID(string $uuid): Issue
    {
        $model = $this->findBy('uuid', $uuid);
        try {
            $model->delete();
            return $model;
        } catch (\Exception $exception) {
            throw new RepositoryException($exception->getMessage());
        }
    }

    public function findByType(string $type, array $relationships = ['category'], array $columns = ['*']): Collection
    {
        return $this->model->with($relationships)->where('type', $type)->get($columns);
    }

}
