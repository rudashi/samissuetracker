<?php

namespace Totem\SamIssueTracker\App\Controllers;

use Illuminate\Http\Request;
use Totem\SamCore\App\Resources\ApiCollection;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamCore\App\Services\DataTableFilters;
use Totem\SamIssueTracker\App\Enums\IssueMorph;
use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Resources\CategoryResource;
use Totem\SamIssueTracker\App\Resources\TreeCategoryResource;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamIssueTracker\App\Repositories\Contracts\CategoryRepositoryInterface;

class CategoryControllers extends ApiController
{

    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request): ApiCollection
    {
        return CategoryResource::collection(
            $this->repository->findByQuery($request->query('query'))
        );
    }

    public function tree(): ApiCollection
    {
        return TreeCategoryResource::collection($this->repository->tree());
    }

    public function rebuildTree(): void
    {
        $this->repository->rebuildTree();
    }

    public function show(int $id): CategoryResource
    {
        return new CategoryResource($this->repository->find($id));
    }

    public function fetchFilters(Request $request): ApiResource
    {
        $table = new DataTableFilters(
            $request->query('view'),
        );
//        $table->setViews($this->repository->getTypes());
        $table->setStatus(StatusType::toSelectArray());
        $table->setTypes(IssueMorph::toSelectArray());

        return new ApiResource($table);
    }

}
