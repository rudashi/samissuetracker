<?php

namespace Totem\SamIssueTracker\App\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Totem\SamIssueTracker\App\Requests\IssueRequest;
use Totem\SamIssueTracker\App\Resources\IssueSimpleCollection;
use Totem\SamIssueTracker\App\Resources\IssueResource;
use Totem\SamIssueTracker\App\Repositories\Contracts\IssueRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class IssueControllers extends ApiController
{

    public function __construct(IssueRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(): IssueSimpleCollection
    {
        return new IssueSimpleCollection(
            $this->getFromRequestQuery($this->repository->allWithAncestors())
        );
    }

    public function create(IssueRequest $request): IssueResource
    {
        return new IssueResource($this->repository->storeNew($request->validated()));
    }

    public function show(string $uuid): IssueResource
    {
        $data = $this->repository->findWithRelationsById($uuid, ['category', 'user']);

        if ($data->category === null) {
            throw new ModelNotFoundException();
        }

        return new IssueResource($data);
    }

    public function update(IssueRequest $request, string $uuid): IssueResource
    {
        return new IssueResource(
            $this->repository->updateByUUID($request, $uuid)
        );
    }

    public function destroy(string $uuid) : IssueResource
    {
        return new IssueResource($this->repository->deleteUUID($uuid));
    }
}
