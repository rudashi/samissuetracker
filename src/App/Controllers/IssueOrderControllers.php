<?php

namespace Totem\SamIssueTracker\App\Controllers;

use Totem\SamIssueTracker\App\Resources\IssueOrderResource;
use Totem\SamIssueTracker\App\Repositories\Contracts\IssueRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class IssueOrderControllers extends ApiController
{

    public function __construct(IssueRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function show(int $order_id): \Totem\SamCore\App\Resources\ApiCollection
    {
        return IssueOrderResource::collection($this->repository->findByOrder($order_id));
    }

}
