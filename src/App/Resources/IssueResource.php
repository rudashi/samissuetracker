<?php

namespace Totem\SamIssueTracker\App\Resources;

use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property Issue $resource
 */
class IssueResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id'                => $this->resource->uuid,
            'created'           => $this->resource->created_at->format('Y-m-d'),
            'created_at'        => $this->resource->created_at->format('Y-m-d H:i:s'),
            'number'            => $this->resource->number,
            'title'             => $this->resource->title,
            'description'       => $this->resource->description,
            'priority'          => $this->resource->priority,
            'status'            => $this->when($this->resource->status !== null, function() {
                return [
                    'value' => $this->resource->status->value,
                    'text' => $this->resource->status->description,
                ];
            }, ['value' => null, 'text' => null]),
            'type'              => [
                'value' => $this->resource->type,
                'text'  => $this->resource->category->view_name,
            ],
            'user' => $this->whenLoaded('user', function() {
                return [
                    'id'            => $this->resource->user_id,
                    'fullname'      => $this->resource->user->fullname,
                ];
            }),
            'order_id'          => $this->resource->order_id,
            'order_number'      => $this->resource->order_number,
            'order_name'        => $this->resource->order_name,
            'order_customer'    => $this->resource->order_customer,
            'workplace'         => $this->when($this->resource->workplace_id !== null, function() {
                return [
                    'value' => $this->resource->workplace_id->value,
                    'text' => $this->resource->workplace_id->description,
                ];
            }),
            'category' => $this->whenLoaded('category', function() {
                return [
                    'id' => $this->resource->category->id,
                    'name' => $this->resource->category->name,
                ];
            }),
            'parameters'    => $this->resource->parameters,
            'update'        => $this->resource->process,
            'processFields' => $this->when(
                $this->resource->relationLoaded('category')
                && !in_array('processFields', $this->resource->category->getHidden(), true),
                $this->resource->category->processFields
            ),
        ];
    }

}
