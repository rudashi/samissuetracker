<?php

namespace Totem\SamIssueTracker\App\Resources;

use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property Issue $resource
 */
class IssueOrderResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id'                => $this->resource->uuid,
            'created_at'        => $this->resource->created_at->format('Y-m-d'),
            'number'            => $this->resource->number,
            'title'             => $this->resource->title,
            'status'            => $this->resource->status->description ?? null,
            'type'              => $this->resource->category->view_name,
            'order_id'          => $this->resource->order_id,
            'workplace'         => $this->resource->workplace_id->description ?? null,
        ];
    }

}
