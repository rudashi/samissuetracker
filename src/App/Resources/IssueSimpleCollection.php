<?php

namespace Totem\SamIssueTracker\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class IssueSimpleCollection extends ApiCollection
{

    public $collects = IssueSimpleResource::class;

}
