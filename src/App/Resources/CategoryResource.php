<?php

namespace Totem\SamIssueTracker\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamIssueTracker\App\Model\Category;

/**
 * @property Category $resource
 */
class CategoryResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'description' => $this->resource->description,
            'type' => $this->resource->getBaseTypeAttribute(),
            'pathInfo' => [
                'pathInfoId' => $this->resource->ancestors->pluck('id')->push($this->resource->id),
                'pathInfoName' => $this->resource->ancestors->pluck('name')->push($this->resource->name),
            ],
            'customFields' => $this->when(!in_array('customFields', $this->resource->getHidden(), true), $this->resource->customFields),
        ];
    }

}
