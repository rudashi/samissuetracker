<?php

namespace Totem\SamIssueTracker\App\Resources;

use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property Issue $resource
 */
class IssueSimpleResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id'                => $this->resource->uuid,
            'created'           => $this->resource->created_at->format('Y-m-d'),
            'number'            => $this->resource->number,
            'order_id'          => $this->resource->order_id,
            'order_number'      => $this->resource->order_number,
            'priority'          => $this->resource->priority,
            'modifiable'        => $this->resource->modifiable,
            'status'            => $this->when($this->resource->status !== null, function() {
                return [
                    'value' => $this->resource->status->value,
                    'text' => $this->resource->status->description,
                ];
            }, ['value' => null, 'text' => null]),
            'type' => $this->whenLoaded('category', function() {
                return [
                    'value' => $this->resource->type,
                    'text'  => __($this->resource->category->view_name),
                ];
            }, $this->resource->type),
            'category' => $this->whenLoaded('category', function() {
                return $this->when($this->resource->category->relationLoaded('ancestors') === false,
                    [
                        'id' => $this->resource->category->id,
                        'name' => $this->resource->category->name,
                    ]
                );
            }),
            'path' => $this->when($this->resource->relationLoaded('category') && $this->resource->category->relationLoaded('ancestors'), function() {
                return $this->resource->category->ancestors->pluck('name')->push($this->resource->category->name);
            }),
        ];
    }

}
