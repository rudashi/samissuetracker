<?php

namespace Totem\SamIssueTracker\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamIssueTracker\App\Model\Category;

/**
 * @property Category $resource
 */
class TreeCategoryResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'children' => self::collection($this->resource->children),
        ];
    }

}
