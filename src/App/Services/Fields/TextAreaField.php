<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

use Totem\SamIssueTracker\App\Enums\CustomFieldType;

class TextAreaField extends CustomField
{

    public function __construct($id, $name, $description = null)
    {
        $this->setType(CustomFieldType::getKey(CustomFieldType::TEXTAREA));

        parent::__construct($id, $name, $description);
    }

}
