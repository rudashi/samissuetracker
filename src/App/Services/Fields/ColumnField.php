<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

use Totem\SamIssueTracker\App\Enums\CustomFieldType;

class ColumnField extends CustomField
{

    public function __construct($id, $name, $required = null)
    {
        $this->setRequired($required ?? true);
        $this->setType(CustomFieldType::getKey(CustomFieldType::COLUMN));

        parent::__construct($id, $name);
    }

}
