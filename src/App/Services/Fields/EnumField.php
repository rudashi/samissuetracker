<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

use Totem\SamIssueTracker\App\Enums\CustomFieldType;

class EnumField extends CustomField
{

    public function __construct($id, $name, ?array $items = [], $description = null)
    {
        $this->setType(CustomFieldType::getKey(CustomFieldType::ENUM));
        $this->setItems($items);

        parent::__construct($id, $name, $description);
    }

}
