<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

use JsonSerializable;
use Totem\SamCore\App\Traits\Jsonable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable as BaseJsonable;

class FieldCollection implements BaseJsonable, Arrayable, JsonSerializable
{
    use Jsonable;

    private array $fields;

    public function __construct(CustomField ...$fields)
    {
        foreach ($fields as $index => $field) {
            $this->fields[$index] = $field;
        }
    }

    public function toArray(): array
    {
        return array_map(static function (CustomField $value) {
            return array_filter(get_object_vars($value));
        }, $this->fields);
    }

    public function jsonSerialize() : array
    {
        return $this->toArray();
    }

}
