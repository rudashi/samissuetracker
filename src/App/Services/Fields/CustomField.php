<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

abstract class CustomField
{

    public string $id;
    public string $name;
    public ?string $description = null;
    public string $type;
    public bool $required = false;
    public ?string $prefix = null;
    public ?string $suffix = null;
    public array $items = [];
    public ?string $extra = null;

    public function __construct($id, $name, $description = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    public function required(): CustomField
    {
        return $this->setRequired(true);
    }

    public function setId(string $id): CustomField
    {
        $this->id = $id;
        return $this;
    }

    public function setRequired(bool $required): CustomField
    {
        $this->required = $required;
        return $this;
    }

    public function setSuffix(?string $suffix): CustomField
    {
        $this->suffix = $suffix;
        return $this;
    }

    public function setPrefix(?string $prefix): CustomField
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function setType(string $type): CustomField
    {
        $this->type = $type;
        return $this;
    }

    public function setItems(?array $items): CustomField
    {
        $this->items = $items ?? [];
        return $this;
    }

    public static function make($id, $name, $description = null)
    {
        return new static($id, $name, $description);
    }

    public function setExtra(?string $extra): CustomField
    {
        $this->extra = $extra;
        return $this;
    }

}
