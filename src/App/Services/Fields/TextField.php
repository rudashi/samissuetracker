<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

use Totem\SamIssueTracker\App\Enums\CustomFieldType;

class TextField extends CustomField
{

    public function __construct($id, $name, $description = null)
    {
        $this->setType(CustomFieldType::getKey(CustomFieldType::TEXT));

        parent::__construct($id, $name, $description);
    }

}
