<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

use Totem\SamIssueTracker\App\Enums\CustomFieldType;

class NumberField extends CustomField
{

    public ?float $min = null;
    public ?float $max = null;
    public ?float $step = null;

    public function __construct($id, $name, $description)
    {
        $this->setType(CustomFieldType::getKey(CustomFieldType::NUMBER));

        parent::__construct($id, $name, $description);
    }

    public function step(?float $step): NumberField
    {
        $this->step = $step;
        return $this;
    }

    public function min(?float $min): NumberField
    {
        $this->min = $min;
        return $this;
    }

    public function max(?float $max): NumberField
    {
        $this->max = $max;
        return $this;
    }


}
