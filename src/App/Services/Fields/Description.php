<?php

namespace Totem\SamIssueTracker\App\Services\Fields;

use Totem\SamIssueTracker\App\Enums\CustomFieldType;

class Description extends CustomField
{

    public function __construct($name)
    {
        $this->setType(CustomFieldType::getKey(CustomFieldType::DESCRIPTION));

        parent::__construct('', $name);
    }

}
