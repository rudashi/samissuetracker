<?php

namespace Totem\SamIssueTracker\App\Listeners;

use Rudashi\BookGenerator\App\Classes\Book;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface;
use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Model\Issue;

class BlockThicknessMeasurementSaving
{

    private JobTicketRepositoryInterface $jobTicket;
    private Book $book;

    public function __construct(JobTicketRepositoryInterface $jobTicket, Book $book)
    {
        $this->jobTicket = $jobTicket;
        $this->book = $book;
    }

    public function handle(Issue $issue): void
    {
        if ($issue->exists) {
            $this->updating($issue);
        } else {
            $this->creating($issue);
        }
    }

    private function creating(Issue $issue): void
    {
        /** @var Issue $model */
        $model = $issue::query()->where('order_id', $issue->order_id)->where('type', 'BlockThicknessMeasurement')->first();

        if ($model) {
            $issue->exists = true;
            $attrs = $model->attributesToArray();
            $attrs['process'] = $attrs['process'] ? json_encode($attrs['process']) : null;
            $attrs['payload'] = json_encode($this->setPayload(
                ['measure' => $this->getOriginalBlockThickness($model)],
                array_merge($this->getPayloadWithoutMeasure($model->getAttribute('payload')), array_values($issue->payload))
            ));
            $issue->setRawAttributes($attrs);
        } else {
            $issue->setAttribute('payload', $this->setPayload(
                ['measure' => $this->getBlockThicknessFromOrder($issue->order_id)],
                array_values($issue->payload)
            ));
        }
    }

    private function updating(Issue $issue): void
    {
        if ($issue->getOriginal('status') === StatusType::COMPLETED) {
            throw new \RuntimeException('Cannot update finished issue.');
        }
        $issue->status = StatusType::COMPLETED;
    }

    private function getPayloadWithoutMeasure(array $payload): array
    {
        unset($payload['measure']);
        return array_values($payload);
    }

    private function setPayload(array $base, array $original): array
    {
        return array_merge($base, array_combine(array_map(static fn($i) => 'measure_'.++$i, array_keys($original)), $original));
    }

    private function getOriginalBlockThickness(Issue $issue): float
    {
        return array_key_exists('measure', $issue->getAttribute('payload'))
            ? $issue->getAttribute('payload')['measure']
            : $this->getBlockThicknessFromOrder($issue->order_id);
    }

    private function getBlockThicknessFromOrder(int $order_id): ?float
    {
        $requestCover = $this->jobTicket->create($order_id)->covers->get(CoverType::getKey(CoverType::COVER));

        if ($requestCover) {
            return $this->book->create(
                new \Illuminate\Http\Request((array) json_decode(json_encode($requestCover), true))
            )->binding->block_thickness;
        }
        return null;

    }
}
