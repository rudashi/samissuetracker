<?php

namespace Totem\SamIssueTracker\App\Listeners;

use Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface;
use Totem\SamIssueTracker\App\Model\Issue;

class OrderIssueCreating
{

    private JobTicketRepositoryInterface $jobTicket;

    public function __construct(JobTicketRepositoryInterface $jobTicket)
    {
        $this->jobTicket = $jobTicket;
    }

    public function handle(Issue $issue): void
    {
        $issue->setAttribute('payload', array_merge([
            'technologist' => $this->jobTicket->create($issue->order_id)->technologist->user_name],
            $issue->payload
        ));
    }

}
