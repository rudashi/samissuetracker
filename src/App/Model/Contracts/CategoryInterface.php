<?php

namespace Totem\SamIssueTracker\App\Model\Contracts;

use Totem\SamIssueTracker\App\Services\Fields\FieldCollection;

interface CategoryInterface
{

    public function getCustomFieldsAttribute(): FieldCollection;

    public function getViewNameAttribute(): string;

}
