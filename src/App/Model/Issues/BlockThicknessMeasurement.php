<?php

namespace Totem\SamIssueTracker\App\Model\Issues;

use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Listeners\BlockThicknessMeasurementSaving;
use Totem\SamIssueTracker\App\Model\Issue;

class BlockThicknessMeasurement extends Issue
{

    protected static array $parameters = [
        'measure',
        'measure_1',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        self::$parameters_intersect = false;
    }

    public function getModifiableAttribute(): bool
    {
        return $this->getAttribute('status')->value !== StatusType::COMPLETED;
    }

    protected static function boot(): void
    {
        parent::boot();

        static::saving(BlockThicknessMeasurementSaving::class);

    }
}
