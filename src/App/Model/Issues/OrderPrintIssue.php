<?php

namespace Totem\SamIssueTracker\App\Model\Issues;

use Totem\SamIssueTracker\App\Enums\Workplace;

class OrderPrintIssue extends OrderIssue
{

    protected static int $workplace = Workplace::PRINT;

}
