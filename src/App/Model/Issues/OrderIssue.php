<?php

namespace Totem\SamIssueTracker\App\Model\Issues;

use Illuminate\Support\Facades\Notification;
use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Listeners\OrderIssueCreating;
use Totem\SamIssueTracker\App\Model\Issue;
use Totem\SamIssueTracker\App\Notifications\OrderIssueCreated;
use Totem\SamIssueTracker\App\Notifications\OrderIssueUpdated;

class OrderIssue extends Issue
{

    protected static array $parameters = [
        'task_id',
        'description',
        'technologist',
    ];

    protected static int $workplace;

    public function getModifiableAttribute(): bool
    {
        if (in_array($this->getAttribute('status')->value, [StatusType::COMPLETED, StatusType::REFUSED], true) === false) {
            return auth()->user()->can(self::technology_perm['slug']);
        }
        return false;
    }

    protected static function boot(): void
    {
        parent::boot();

        static::creating(OrderIssueCreating::class);

        static::created(function(Issue $issue) {
            Notification::route('mail', [
                config('sam-issue-tracker.emails.'.$issue->workplace_id),
                config('sam-issue-tracker.emails.'.static::$workplace),
                config('sam-issue-tracker.emails.technology'),
            ])->notify(new OrderIssueCreated($issue));
        });

        static::updating(function(Issue $issue) {
            if (in_array($issue->getOriginal('status'), [StatusType::COMPLETED, StatusType::REFUSED], true)) {
                throw new \RuntimeException('Cannot update finished issue.');
            }
        });

        static::updated(function(Issue $issue) {
            Notification::route('mail', [
                config('sam-issue-tracker.emails.'.$issue->workplace_id),
                config('sam-issue-tracker.emails.'.static::$workplace)
            ])->notify(new OrderIssueUpdated($issue));
        });
    }
}
