<?php

namespace Totem\SamIssueTracker\App\Model\Issues;

use Totem\SamIssueTracker\App\Enums\Workplace;

class OrderBinderyIssue extends OrderIssue
{

    protected static int $workplace = Workplace::BINDERY;

}
