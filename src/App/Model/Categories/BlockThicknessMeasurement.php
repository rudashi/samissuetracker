<?php

namespace Totem\SamIssueTracker\App\Model\Categories;

use Totem\SamIssueTracker\App\Model\Contracts\CategoryInterface;
use Totem\SamIssueTracker\App\Services\Fields\NumberField;
use Totem\SamIssueTracker\App\Services\Fields\ColumnField;
use Totem\SamIssueTracker\App\Services\Fields\Description;
use Totem\SamIssueTracker\App\Services\Fields\FieldCollection;
use Totem\SamIssueTracker\App\Model\Category;

class BlockThicknessMeasurement extends Category implements CategoryInterface
{

    public function getCustomFieldsAttribute(): FieldCollection
    {
        return new FieldCollection(
            new Description('Pomiar należy dokonać na wyzerowanej suwmiarce oraz bez użycia przesadnie dużej siły. Pomiaru proszę dokonywać z dołu lub przodu bloku., tak aby szczęki suwmiarki nie nachodziły na druk ani klejenie bookline. Uwaga!! Jeżeli wnętrze składa się z dwóch lub 3 części należy dla każdej wypełnić formularz z oznaczeniem części 1123/2020/A lub 1123/2020/B itd'),
            new ColumnField('order_id', 'Numer zlecenia'),
            NumberField::make('measure_1', 'Pomiar 1', 'Pomiar na początku produkcji')->required()->setSuffix('mm')->step(0.01),
            NumberField::make('measure_2', 'Pomiar 2')->setSuffix('mm')->step(0.01),
            NumberField::make('measure_3', 'Pomiar 3')->setSuffix('mm')->step(0.01),
            NumberField::make('measure_4', 'Pomiar 4')->setSuffix('mm')->step(0.01),
        );
    }

    public function getViewNameAttribute(): string
    {
        return 'Block thickness measurement';
    }

    public function getProcessFieldsAttribute(): FieldCollection
    {
        return new FieldCollection(
            NumberField::make('verified_measure', 'Pomiar wykonany przez technologa')->required()->setSuffix('mm')->step(0.01),
        );
    }

}
