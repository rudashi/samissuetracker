<?php

namespace Totem\SamIssueTracker\App\Model\Categories;

use Totem\SamIssueTracker\App\Enums\DefectPlaceType;
use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Model\Category;
use Totem\SamIssueTracker\App\Enums\Workplace;
use Totem\SamIssueTracker\App\Services\Fields\ColumnField;
use Totem\SamIssueTracker\App\Services\Fields\Description;
use Totem\SamIssueTracker\App\Services\Fields\EnumField;
use Totem\SamIssueTracker\App\Services\Fields\FieldCollection;
use Totem\SamIssueTracker\App\Services\Fields\TextAreaField;

class OrderIssue extends Category
{

    public static string $workplace;

    public function getCustomFieldsAttribute(): FieldCollection
    {
        $workplace = strtoupper(static::$workplace);

        return new FieldCollection(
            new Description('Podaj problem który napotkałeś przy próbie wykonania zlecenia'),
            new ColumnField('order_id', 'Numer zlecenia'),
            ColumnField::make('workplace_id', 'Stanowisko')->setItems(Workplace::$workplace()->getFlags()),
            EnumField::make('task_id', 'Rodzaj zadania')->required()->setExtra(static::$workplace),
            new TextAreaField('description', 'Dodatkowy opis'),
        );
    }

    public function getProcessFieldsAttribute(): FieldCollection
    {
        return new FieldCollection(
            (new EnumField('defect_place', 'Miejsce powstania błędu', DefectPlaceType::toSelect()))->required(),
            TextAreaField::make('description', 'Dodatkowy opis')->required(),
            ColumnField::make('status', 'Status problemu')->setItems(StatusType::toSelect()),
        );
    }

}
