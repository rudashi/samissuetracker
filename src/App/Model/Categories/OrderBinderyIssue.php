<?php

namespace Totem\SamIssueTracker\App\Model\Categories;

use Totem\SamIssueTracker\App\Model\Contracts\CategoryInterface;

class OrderBinderyIssue extends OrderIssue implements CategoryInterface
{

    public static string $workplace = 'bindery';

    public function getViewNameAttribute(): string
    {
        return 'Bindery Issue';
    }

}
