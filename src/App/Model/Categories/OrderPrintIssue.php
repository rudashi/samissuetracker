<?php

namespace Totem\SamIssueTracker\App\Model\Categories;

use Totem\SamIssueTracker\App\Model\Contracts\CategoryInterface;

class OrderPrintIssue extends OrderIssue implements CategoryInterface
{

    public static string $workplace = 'print';

    public function getViewNameAttribute(): string
    {
        return 'Print Issue';
    }

}
