<?php

namespace Totem\SamIssueTracker\App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Totem\SamCore\App\Traits\EloquentDecoratorTrait;
use Totem\SamIssueTracker\App\Services\Fields\FieldCollection;

/**
 * @property int $id
 * @property string $name
 * @property string $view_name
 * @property string $base_type
 * @property string $description
 * @property FieldCollection $customFields
 * @property \Kalnoy\Nestedset\Collection $ancestors
 * @property \Illuminate\Database\Eloquent\Collection $children
 *
 * @method static Builder ofType(string $type)
 */
class Category extends Model
{
    use EloquentDecoratorTrait,
        SoftDeletes,
        NodeTrait;

    public function __construct(array $attributes = [])
    {
        $this->timestamps = false;

        $this->fillable([
            'id',
            'name',
            'description',
            '_lft',
            '_rgt',
            'parent_id',
            'order',
            'type',
        ]);
        $this->addHidden([
            '_lft',
            '_rgt',
            'parent_id',
            'order',
        ]);

        $this->setTable('issues_categories');

        parent::__construct($attributes);
    }

    public function getBaseTypeAttribute(): string
    {
        return class_basename(static::class);
    }

    public function scopeOfType(Builder $query, string $type): Builder
    {
        return $query->where(['type' => $type]);
    }

}
