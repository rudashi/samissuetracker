<?php

namespace Totem\SamIssueTracker\App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Totem\SamCore\App\Repositories\Contracts\HasFilesInterface;
use Totem\SamCore\App\Traits\CastsEnums;
use Totem\SamCore\App\Traits\EloquentDecoratorTrait;
use Totem\SamCore\App\Traits\HasFiles;
use Totem\SamCore\App\Traits\HasParameters;
use Totem\SamCore\App\Traits\HasUuid;
use Totem\SamIssueTracker\App\Enums\IssueMorph;
use Totem\SamIssueTracker\App\Enums\StatusType;
use Totem\SamIssueTracker\App\Enums\Workplace;

/**
 * @property string uuid
 * @property \DateTime created_at
 * @property int user_id
 * @property int order_id
 * @property string order_number
 * @property string order_name
 * @property string order_customer
 * @property string number
 * @property Workplace|null workplace_id
 * @property StatusType|null status
 * @property string title
 * @property string description
 * @property int priority
 * @property array payload
 * @property array|null process
 * @property string|null type
 * @property array parameters
 * @property bool modifiable
 * @property Category category
 * @property \Illuminate\Contracts\Auth\Authenticatable user
 */
class Issue extends Model implements HasFilesInterface
{
    use HasUuid,
        HasFiles,
        SoftDeletes,
        CastsEnums,
        EloquentDecoratorTrait,
        HasParameters;

    protected $casts = [
        'uuid' => 'string',
        'payload' => 'array',
        'process' => 'array',
    ];

    protected array $enums = [
        'workplace_id' => Workplace::class,
        'status'  => StatusType::class,
    ];

    public const technology_perm = [
        'slug' => 'issues.edit.technology',
        'name' => 'Can Update Issue',
        'description' => 'Can update issue status as technology',
    ];

    public function __construct(array $attributes = [])
    {
        $this->fillable([
            'user_id',
            'category_id',
            'title',
            'description',
            'priority',
            'order_id',
            'order_number',
            'order_name',
            'order_customer',
            'workplace_id',
            'payload',
            'process',
            'status',
            'assignee_id',
        ]);

        $this->setKeyName('uuid');
        $this->setTable('issues');

        self::$parameters_column = 'payload';

        parent::__construct($attributes);
    }

    public function getModifiableAttribute(): bool
    {
        return true;
    }

    public function path(): string
    {
        return 'issues';
    }

    public function getNewNumber(): int
    {
        return mt_rand(100000, 999999);
    }

    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(app('config')->get('auth.providers.users.model'))->withTrashed();
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function newFromBuilder($attributes = [], $connection = null) : Model
    {
        return $this->decorator('type', IssueMorph::toArray(), $attributes, $connection);
    }

}
