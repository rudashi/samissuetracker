<?php

namespace Totem\SamIssueTracker\App\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Totem\SamIssueTracker\App\Enums\DefectPlaceType;
use Totem\SamIssueTracker\App\Enums\StatusType;

class OrderIssueRequest implements IssueRequestInterface
{

    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function creating(): array
    {
        return [
            'category_id'           => 'required|integer',
            'order_id'              => 'required|integer',
            'order_number'          => 'required_with:order_id',
            'order_name'            => 'required_with:order_id',
            'order_customer'        => 'required_with:order_id',
            'workplace_id'          => 'required|integer',
            'payload'               => 'required|array',
            'payload.task_id'       => 'required',
            'payload.description'   => 'nullable',
        ];
    }

    public function updating(): array
    {
        return [
            'status' => [
                'required',
                Rule::in(StatusType::toArray()),
            ],
            'process.description' => 'required',
            'process.defect_place' => [
                Rule::requiredIf($this->request->input('status') === StatusType::COMPLETED),
                Rule::in(DefectPlaceType::toArray()),
            ],
        ];
    }
}
