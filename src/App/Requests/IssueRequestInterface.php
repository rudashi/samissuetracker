<?php

namespace Totem\SamIssueTracker\App\Requests;

interface IssueRequestInterface
{

    public function creating(): array;

    public function updating(): array;

}
