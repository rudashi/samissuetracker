<?php

namespace Totem\SamIssueTracker\App\Requests;

use Illuminate\Support\Str;
use Totem\SamCore\App\Requests\BaseRequest;

class IssueRequest extends BaseRequest
{

    private ?IssueRequestInterface $type = null;

    public function rules(): array
    {
        if ($this->type === null) {
            return [ 'type' => 'required'];
        }
        switch ($this->method()) {
            case 'POST':
                return $this->type->creating();
            case 'PATCH':
                return $this->type->updating();
            default:
                return [];
        }
    }

    protected function prepareForValidation(): void
    {
        if ($this->has('type')) {
            $request = $this->getRequestClass();

            if (class_exists($request)) {
                $this->type = app($request);
                if (method_exists($this->type, 'prepareForValidation')) {
                    $this->type->prepareForValidation($this);
                }
            } else {
                $this->merge(['type' => null]);
            }
        }
    }

    private function getRequestClass(): string
    {
        return Str::contains($this->post('type'), '\\')
            ? $this->post('type')
            : __NAMESPACE__. '\\' .$this->post('type').'Request';
    }

}
