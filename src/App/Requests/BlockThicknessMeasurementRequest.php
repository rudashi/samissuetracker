<?php

namespace Totem\SamIssueTracker\App\Requests;

class BlockThicknessMeasurementRequest implements IssueRequestInterface
{

    public function creating(): array
    {
        return [
            'category_id'           => 'required|integer',
            'order_id'              => 'required|integer',
            'order_number'          => 'required_with:order_id',
            'order_name'            => 'required_with:order_id',
            'order_customer'        => 'required_with:order_id',
            'payload'               => 'required|array',
            'payload.measure_1'     => 'required|numeric',
            'payload.measure_2'     => 'nullable|numeric',
            'payload.measure_3'     => 'nullable|numeric',
            'payload.measure_4'     => 'nullable|numeric',
        ];
    }

    public function updating(): array
    {
        return [
            'process.verified_measure' => 'required|numeric',
        ];
    }

}
