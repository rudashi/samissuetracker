Sam - Issue Tracking System
================

This package manages issues in SAM application.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

General System Requirements
-------------
- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-Admin ~1.*](https://bitbucket.org/rudashi/samadmin)
- [SAM-Acl ~1.*](https://bitbucket.org/rudashi/samacl)  

Quick Installation
-------------
If necessary, use the composer to download the library

```
$ composer require totem-it/sam-sam-issue-tracker
```

Remember to put repository in the composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/samissuetracker.git"
    }
],
```

Usage
-------------

###API

Get categories as tree collection.
```
GET /issues/categories/tree

permission: issue-tracker.view
```

Search categories by a phrase.
```
GET /issues/categories?query={phrase}

permission: issue-tracker.view
```

Authors
-------------

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
